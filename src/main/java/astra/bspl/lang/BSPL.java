package astra.bspl.lang;

import java.util.Map.Entry;

import astra.bspl.ProtocolAdaptor;
import astra.bspl.ProtocolExecutor;
import astra.bspl.ProtocolLibrary;
import astra.bspl.ProtocolListener;
import astra.bspl.model.Message;
import astra.bspl.model.MessageSchema;
import astra.bspl.model.Parameter;
import astra.bspl.model.ParameterSet;
import astra.bspl.model.ParticipantSet;
import astra.bspl.transport.LocalProtocolTransport;
import astra.bspl.transport.ProtocolTransport;
import astra.core.Agent;
import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.reasoner.Unifier;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class BSPL extends Module {
    static {
		Unifier.eventFactory.put(BSPLEvent.class, new BSPLEventUnifier());
	}

    private ProtocolLibrary library = new ProtocolLibrary();
    private ProtocolTransport transport = new LocalProtocolTransport();
    private static int counter = 0;
    private ProtocolAdaptor adaptor = null;

    public BSPL(String transportClass) {
        try {
            transport = (ProtocolTransport) Class.forName(transportClass).newInstance();
            transport.configure(new ParameterSet());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to instantiate protocol: " + transportClass, e);
        }
    }

    @SuppressWarnings("unchecked")
    public BSPL(String transportClass, ListTerm configuration) {
        ParameterSet set = new ParameterSet();
        for (Term parameter : configuration) {
            Funct funct = (Funct)  parameter;
            set.set(funct.functor(),((Primitive<String>) funct.termAt(0)).value());
        }

        try {
            transport = (ProtocolTransport) Class.forName(transportClass).newInstance();
            transport.configure(set);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to instantiate protocol: " + transportClass, e);
        }
    }

    private BSPL(ProtocolTransport transport) {
        this.transport = transport;
        transport.configure(new ParameterSet());
    }

    public BSPL() {
        this(new LocalProtocolTransport());
    }

    public void setAgent(Agent agent) {
        super.setAgent(agent);
        adaptor = new ProtocolAdaptor(agent.name(), library, transport);
        adaptor.setListener(new ProtocolListener() {
            @Override
            public void protocolEnacted(ProtocolExecutor executor) {
                agent.addEvent(new BSPLEvent(
                    Primitive.newPrimitive("enacted"),
                    Primitive.newPrimitive(executor)));
            }

            @Override
            public void protocolActivated(ProtocolExecutor executor) {
                agent.addEvent(new BSPLEvent(Primitive.newPrimitive("activated"), Primitive.newPrimitive(executor), Primitive.newPrimitive(executor.getLastMessage())));
            }
        });
    }

    @TERM
    public String createId() {
        return agent.name()+"_"+(counter++);
    }
    
    @ACTION
    public boolean loadProtocol(String filename) {
        // System.out.println("["+agent.name()+"] Loading Protocol: " + filename);
        library.loadProtocol(filename);
        return true;
    }

    // @ACTION
    // public boolean addOutParameter(ProtocolExecutor executor, String param, String value) {
    //     executor.getOutParameters().set(param, value);
    //     return true;
    // }

    @ACTION
    public boolean enactProtocol(String protocolName, String role, ListTerm participants) {
        adaptor.enactProtocol(protocolName, role, createParticipantSet(participants));
        return true;
    }

    @SuppressWarnings("unchecked")
    private ParticipantSet createParticipantSet(ListTerm participants) {
        ParticipantSet set = new ParticipantSet();
        
        for (Term participant : participants) {
            Funct funct = (Funct) participant;
            set.set(funct.functor(),((Primitive<String>) funct.termAt(0)).value());
        }
        
        return set;
    }

    @FORMULA
    public Formula hasCandidateSenderSchemes(ProtocolExecutor executor) {
        return executor.getCandidateSenderSchemas(executor.getRole()).isEmpty() ? Predicate.TRUE:Predicate.FALSE;
    }

    @SuppressWarnings("unchecked")
    private ParameterSet createParameterSet(ProtocolExecutor executor, ListTerm parameters) {
        // Use any existing defined out parameters and reset the out
        // parameter set for the next message...
        ParameterSet set = new ParameterSet();

        for (Term parameter : parameters) {
            Funct funct = (Funct)  parameter;
            if ((funct.size() == 2) && funct.functor().equals("param"))
                set.set(((Primitive<String>) funct.termAt(0)).value(), ((Primitive<String>) funct.termAt(1)).value());
            else if (funct.size() == 1)
                set.set(funct.functor(),((Primitive<String>) funct.termAt(0)).value());
            else
                throw new RuntimeException("Invalid parameter specfication: " + funct);
        }
        return set;
    }

    @ACTION
    public boolean emit(ProtocolExecutor executor, String type, ListTerm parameters) {
        Message message = executor.createMessage(type, createParameterSet(executor, parameters));
        // System.out.println("["+agent.name()+"] SENDING: "  + Utils.toJsonString(message));
        executor.send(message);
        return true;
    }

    @ACTION
    public boolean emit(String protocol, String role, ListTerm participants, String type, ListTerm parameters) {
        ProtocolExecutor executor = adaptor.enactProtocol(protocol, role, createParticipantSet(participants));
        return emit(executor, type, parameters);
    }

    @FORMULA
    public Formula isProtocol(ProtocolExecutor executor, String name) {
        return executor.getProtocol().getName().equals(name) ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula isParameter(ProtocolExecutor executor, String item, String value) {
        return executor.getParameters().get(item).equals(value) ? Predicate.TRUE:Predicate.FALSE;
    }

    @TERM
    public String getParameter(ParameterSet set, String key) {
        return set.get(key);
    }

    @ACTION
    public boolean addParameter(ProtocolExecutor executor, String key, String value) {
        executor.getParameters().set(key, value);
        return true;
    }
    @TERM
    public String getParameter(ProtocolExecutor executor, String key) {
        return executor.getParameters().get(key);
    }

    @FORMULA
    public Formula hasParameter(ProtocolExecutor executor, String param) {
        return executor.getParameters().get(param) == null ? Predicate.FALSE:Predicate.TRUE;
    }

    @TERM 
    public ListTerm getParameterKeys(ParameterSet set)  {
        ListTerm list = new ListTerm();
        for(String key : set.getParameters().keySet()) {
            list.add(Primitive.newPrimitive(key));
        }
        return list;
    }

    @TERM
    public ListTerm getParticipants(ProtocolExecutor executor) {
        ListTerm participants = new ListTerm();
        for (Entry<String, String> entry : executor.getParticipants().getParticipants().entrySet()) {
            participants.add(new Funct(entry.getKey(), new Term[] {Primitive.newPrimitive(entry.getValue())}));
        }
        return participants;
    }

    @FORMULA
    public Formula hasParticipant(ProtocolExecutor executor, String role) {
        return executor.getParticipants().get(role) == null ? Predicate.FALSE:Predicate.TRUE;
    }

    @ACTION
    public boolean addParticipant(ProtocolExecutor executor, String role, String name) {
        executor.getParticipants().set(role, name);
        return true;
    }

    @TERM
    public ListTerm getActiveMessageSchemas(ProtocolExecutor executor) {
        ListTerm schemas = new ListTerm();
        for (MessageSchema schema : executor.getCandidateSenderSchemas(executor.activeRole())) {
            schemas.add(Primitive.newPrimitive(schema.getName()));
        }
        return schemas;
    }

    @TERM
    public String protocol(ProtocolExecutor executor) {
        return executor.getProtocol().getName();
    }

    @TERM
    public ListTerm getOutParametersForMessageSchema(ProtocolExecutor executor, String name) {
        ListTerm parameters = new ListTerm();
        for (Parameter parameter : executor.getSchema(name).getParameters()) {
            if (parameter.getType().equals(Parameter.OUT)) {
                parameters.add(Primitive.newPrimitive(parameter.getName()));
            }
        }
        return parameters;
    }

    @TERM
    public ParameterSet getParameters(ProtocolExecutor executor) {
        return executor.getParameters();
    }

    @FORMULA
    public Formula lastMessage(ProtocolExecutor executor, String type) {
        return executor.getLastMessage().getName().equals(type) ? Predicate.TRUE:Predicate.FALSE;
    }

    @EVENT( types = { "astra.bspl.ProtocolExecutor" }, signature="$bspl:", symbols = {} )
    public BSPLEvent enacted(Term executor) {
        return new BSPLEvent(Primitive.newPrimitive("enacted"), executor);
    }

    @EVENT( types = { "astra.bspl.ProtocolExecutor" }, signature="$bspl:", symbols = {} )
    public BSPLEvent activated(Term executor) {
        return new BSPLEvent(Primitive.newPrimitive("activated"), executor);
    }
}
