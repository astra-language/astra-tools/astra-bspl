package astra.bspl.lang;


import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Primitive;
import astra.term.Term;

public class BSPLEventUnifier implements EventUnifier<BSPLEvent> {
	static final Primitive<String> enacted = Primitive.newPrimitive("enacted");
	@Override
	public Map<Integer, Term> unify(BSPLEvent source, BSPLEvent target, Agent agent) {
		// if (source.type.equals(enacted)) {
			return Unifier.unify(
					new Term[] {source.type, source.executor}, 
					new Term[] {target.type, target.executor}, 
					new HashMap<Integer, Term>(),
					agent);
		// } else {
		// 	return Unifier.unify(
		// 		new Term[] {source.type, source.executor, source.message}, 
		// 		new Term[] {target.type, target.executor, target.message}, 
		// 		new HashMap<Integer, Term>(),
		// 		agent);
		// }
	}


}
