package astra.bspl.lang;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class BSPLEvent implements Event {
    Term type;
    Term executor;
    Term message;
    
    public BSPLEvent(Term type, Term executor, Term message) {
        this.type=type;
        this.executor=executor;
        this.message=message;
    }

    public BSPLEvent(Term type, Term executor) {
        this.type=type;
        this.executor=executor;
    }

    @Override
    public Object getSource() {
        return null;
    }

    @Override
    public String signature() {
        return "$bspl:";
    }

    @Override
    public Event accept(LogicVisitor visitor) {
        if (message != null)
            return new BSPLEvent(
                (Term) type.accept(visitor),
                (Term) executor.accept(visitor),
                (Term) message.accept(visitor));
        else 
        return new BSPLEvent(
            (Term) type.accept(visitor),
            (Term) executor.accept(visitor));

    }    
}
